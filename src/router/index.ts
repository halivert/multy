import { createRouter, createWebHistory } from "vue-router";

export const routes = [
	{
		path: "/",
		name: "home",
		component: () => import("@/pages/Home.vue"),
		text: "Home",
	},
	{
		path: "/battery",
		name: "battery",
		component: () => import("@/pages/BatteryAPI.vue"),
		text: "Battery",
	},
];

const router = createRouter({
	history: createWebHistory(import.meta.BASE_URL),
	routes,
});

export { router };
