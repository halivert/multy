import { ref, watchEffect } from "vue";

export const useBattery = () => {
	const charging = ref(false);
	const chargingTime = ref(0);
	const dischargingTime = ref(0);
	const level = ref(0);

	if (!navigator.getBattery) {
		return {
			charging,
			chargingTime,
			dischargingTime,
			level,
		};
	}

	const updateValue = (event, target, name) => {
		console.log(event.target[name]);
		target.value = event.target[name];
	};

	const upCharging = (e) => updateValue(e, charging, "charging");
	const upLevel = (e) => updateValue(e, level, "level");
	const upChargingTime = (e) => updateValue(e, chargingTime, "chargingTime");
	const upDischargingTime = (e) =>
		updateValue(e, dischargingTime, "dischargingTime");

	navigator.getBattery().then((battery) => {
		charging.value = battery.charging;
		chargingTime.value = battery.chargingTime;
		dischargingTime.value = battery.dischargingTime;
		level.value = battery.level;

		battery.addEventListener("chargingchange", upChargingTime);
		battery.addEventListener("levelchange", upLevel);
		battery.addEventListener("chargingtimechange", upChargingTime);
		battery.addEventListener("dischargingtimechange", upDischargingTime);
	});

	watchEffect((onCleanup) => {
		onCleanup(() => {
			navigator.getBattery().then((battery) => {
				battery.removeEventListener("chargingchange", upCharging);
				battery.removeEventListener("levelchange", upLevel);
				battery.removeEventListener("chargingtimechange", upChargingTime);
				battery.removeEventListener("dischargingtimechange", upDischargingTime);
			});
		});
	});

	return {
		charging,
		chargingTime,
		dischargingTime,
		level,
	};
};
